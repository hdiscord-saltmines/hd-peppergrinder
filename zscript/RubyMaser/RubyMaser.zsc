// ------------------------------------------------------------
// Rubicon AEF-322c Microwave Laser
// Also known as the Ruby Maser
// ------------------------------------------------------------
const HDLD_MASER="mas";
class RubyMaser:HDCellWeapon{
	default{
		weapon.selectionorder 1;
		weapon.slotnumber 6;
		weapon.slotpriority 0.5;
		scale 0.9;
		inventory.pickupmessage "You got the Rubicon AEF-322c Microwave Laser!";
		obituary "%o was boiled by %k's maser beam.";
		hdweapon.barrelsize 32,1.6,3;
		hdweapon.refid HDLD_MASER;
		tag "Ruby Maser";
		
		hdweapon.loadoutcodes "
			\cueject - 0/1, Ejector Model, spits out empty batteries
			\cualt - 0/1, Start on Wide Beam mode";
	}
	override bool AddSpareWeapon(actor newowner){return AddSpareWeaponRegular(newowner);}
	override hdweapon GetSpareWeapon(actor newowner,bool reverse,bool doselect){return GetSpareWeaponRegular(newowner,reverse,doselect);}
	override void tick(){
		super.tick();
		drainheat(RMS_HEAT,12);
	}
	override void postbeginplay(){
		super.postbeginplay();
			weaponspecial=1337;
	}
	override string pickupmessage(){
        if(random(1,40)==1)return string.format("%s Oh jeez it's heavy...",super.pickupmessage());
		if(weaponstatus[0]&RMF_EJECT)return string.format("%s Eject Model!",super.pickupmessage());
		return super.pickupmessage();
	}
	override double gunmass(){
		return 14+(weaponstatus[RMS_BATTERY]<0?0:2)+(weaponstatus[0]&RMF_DOUBLEBAT<0?0:2);
	}
	override double weaponbulk(){
		return 240+(weaponstatus[1]>=0?ENC_BATTERY_LOADED:0)+(weaponstatus[0]&RMF_DOUBLEBAT?ENC_BATTERY_LOADED:0);
	}
	override string,double getpickupsprite(){return "MASEZ0",1.;}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			sb.drawbattery(-54,-4,sb.DI_SCREEN_CENTER_BOTTOM,reloadorder:true);
			sb.drawnum(hpl.countinv("HDBattery"),-46,-8,sb.DI_SCREEN_CENTER_BOTTOM);
		}
		if(hdw.weaponstatus[0]&RMF_ALT)sb.drawimage("BEAMSEL",(-35,-10),sb.DI_SCREEN_CENTER_BOTTOM);
		else sb.drawimage("STEALTHSEL",(-35,-12),sb.DI_SCREEN_CENTER_BOTTOM);
		int bat=hdw.weaponstatus[RMS_BATTERY];
		if(bat>0)sb.drawwepnum(bat,20);
		else if(!bat)sb.drawstring(
			sb.mamountfont,"000000",
			(-16,-9),sb.DI_TEXT_ALIGN_RIGHT|sb.DI_TRANSLATABLE|sb.DI_SCREEN_CENTER_BOTTOM,
			Font.CR_DARKGRAY
		);
		if(hdw.weaponstatus[0]&RMF_DOUBLEBAT){
			if(hdw.weaponstatus[RMS_BATTERY2]>0)sb.drawrect(-20,-12,4,2.6);
			else{
				sb.drawrect(-20,-10,4,0.8);
				sb.drawrect(-20,-12,4,0.8);
				sb.drawrect(-20,-12,0.8,2);
				sb.drawrect(-16.8,-12,0.8,2);
			}
		}
	}
	override string gethelptext(){
		return
		WEPHELP_FIRESHOOT
		..WEPHELP_ALTFIRE.."  Switch to "..(weaponstatus[0]&RMF_ALT?"focused":"unfocused").." mode\n"
		..WEPHELP_RELOADRELOAD
		..WEPHELP_UNLOADUNLOAD
		..WEPHELP_RELOAD.." + "..WEPHELP_USE.." Cycle loaded batteries\n"
		..WEPHELP_ALTRELOAD.."  load backup battery"
		;
	}
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc,string whichdot
	){
		int cx,cy,cw,ch;
		[cx,cy,cw,ch]=screen.GetClipRect();
		sb.SetClipRect(
			-16+bob.x,-4+bob.y,32,16,
			sb.DI_SCREEN_CENTER
		);
		vector2 bobb=bob*1.2;
		sb.drawimage(
			"frntsite",(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			alpha:0.9,scale:(1.6,2)
		);
		sb.SetClipRect(cx,cy,cw,ch);
		sb.drawimage(
			"backsite",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			scale:(2,1)
		);
	}
	override void failedpickupunload(){
		failedpickupunloadmag(RMS_BATTERY,"HDBattery");
	}
	override void consolidate(){
		CheckBFGCharge(RMS_BATTERY);
		CheckBFGCharge(RMS_BATTERY2);
	}
	override void DetachFromOwner(){
		owner.A_StopSound(CHAN_WEAPON);
		Super.DetachFromOwner();
	}
	static void MaserZap(
		actor caller,
		double zoffset=32,
		bool alt=false,
		int battery=20
	){
		double shootangle=caller.angle;
		double shootpitch=caller.pitch;
		let hdp=hdplayerpawn(caller);
		if(hdp&&hdp.scopecamera){
			shootangle=hdp.scopecamera.angle;
			shootpitch=hdp.scopecamera.pitch;
		}
		if(alt){
			shootangle+=frandom(-2.4,2.4);
			shootpitch+=frandom(-2.5,2.2);
		}
		
		flinetracedata tlt;
		caller.linetrace(
			shootangle,
			8000+200*battery,
			shootpitch,
			flags:TRF_NOSKY|TRF_THRUBLOCK,
			offsetz:zoffset,
			data:tlt
		);
		if(alt){
			if(tlt.hittype==Trace_HitNone||tlt.distance>6000)return;
			actor bbb=spawn("MaserHigh",tlt.hitlocation-tlt.hitdir,ALLOW_REPLACE);
			MaserLow(bbb).impactdistance=tlt.distance-16*battery;
			bbb.angle=caller.angle;
			bbb.pitch=caller.pitch;
			bbb.target=caller;
			bbb.tracer=tlt.hitactor;
			return;
		}else{
			if(tlt.hittype==Trace_HitNone||tlt.distance>6000)return;
			actor bbb=spawn("MaserLow",tlt.hitlocation-tlt.hitdir,ALLOW_REPLACE);
			MaserLow(bbb).impactdistance=tlt.distance-16*battery;
			bbb.angle=caller.angle;
			bbb.pitch=caller.pitch;
			bbb.target=caller;
			bbb.tracer=tlt.hitactor;
			return;
		}
		int basedmg=int(max(0,20-tlt.distance*(1./50.)));
		int dmgflags=caller&&caller.player?DMG_PLAYERATTACK:0;

		if(tlt.hitactor){
			actor hitactor=tlt.hitactor;
			if(hitactor.bloodtype=="ShieldNotBlood"){
				hitactor.damagemobj(null,caller,random(1,(battery<<2)),"Balefire",dmgflags);
			}
		}
		actor bbb=spawn("MaserSpot",tlt.hitlocation-tlt.hitdir,ALLOW_REPLACE);
		bbb.target=caller;
		bbb.stamina=basedmg;
		bbb.angle=caller.angle;
		bbb.pitch=caller.pitch;
	}
	action void A_MaserZap(){
		if(invoker.weaponstatus[RMS_HEAT]>20)return;
		int battery=invoker.weaponstatus[RMS_BATTERY];
		int battery2=invoker.weaponstatus[RMS_BATTERY2];
		if(battery<1&&battery2<1){
			setweaponstate("nope");
			return;
		}
		A_ZoomRecoil(0.99);
		if(invoker.weaponstatus[0]&RMF_ALT)A_StartSound("weapons/rubyfirehi",CHAN_WEAPON);
		else A_StartSound("weapons/rubyfirelo",CHAN_WEAPON);
		if(countinv("IsMoving")>9)A_MuzzleClimb(frandom(-0.05,0.05),frandom(-0.05,0.05));
        vector3 gpos=HDMath.GetGunPos(self);
		RubyMaser.MaserZap(
			self,
			gpos.z,
			invoker.weaponstatus[0]&RMF_ALT,
			battery 
		);
		if(invoker.weaponstatus[0]&RMF_ALT){
			if(!random(0,5))invoker.weaponstatus[RMS_BATTERY]--;
			invoker.weaponstatus[RMS_HEAT]+=random(0,3);
		}else if(!random(0,10))invoker.weaponstatus[RMS_BATTERY]--;
		invoker.weaponstatus[RMS_HEAT]+=random(0,3);
	}
	action void A_MaserActionCheck(){
		if(PressingFire())setweaponstate("hold");
		if(PressingFiremode())setweaponstate("firemode");
		if(PressingReload())setweaponstate("reload");
		if(PressingUnload())setweaponstate("unload");
	}
	states{
	ready:
		MASE A 1{
			A_CheckIdSprite("MASEA0","MASEA0");
			A_SetCrosshair(21);
			if(justpressed(BT_USER1))setweaponstate("altreload");
			A_WeaponReady(WRF_ALL&~WRF_ALLOWUSER1);
			}
		goto readyend;
		MASE ABCDEFG 0;
	fire:
		#### ABC 3 offset(0,35);
	hold:
		#### A 0 A_JumpIf(invoker.weaponstatus[RMS_BATTERY]>0,"shoot2");
		---- A 0 A_StartSound("weapons/rubyendlo",CHAN_WEAPON);
		goto nope;
	shoot:
		#### C 0 {if(invoker.weaponstatus[0]&RMF_ALT)A_StartSound("weapons/rubycharge",CHAN_WEAPON,2.0);}
	shoot2:
		#### D 1 offset(1,33){
			A_MaserZap();
			if(invoker.weaponstatus[0]&RMF_ALT){A_MaserZap();A_MaserZap();A_MaserZap();A_MaserZap();A_MaserZap();A_MaserZap();A_MaserZap();}
			A_GunFlash();
			}
		#### E 1 offset(0,34) A_WeaponReady(WRF_NONE);
		#### F 1 offset(-1,33) A_WeaponReady(WRF_NONE);
		#### A 0{
			if(invoker.weaponstatus[RMS_BATTERY]<1){
				if(invoker.weaponstatus[0]&RMF_ALT)A_StartSound("weapons/rubyendhi",CHAN_WEAPON);
				else A_StartSound("weapons/rubyendlo",CHAN_WEAPON);
				A_GunFlash();
				if(invoker.weaponstatus[0]&RMF_DOUBLEBAT&&RMS_BATTERY2>1)setweaponstate("batswap");
				else setweaponstate("nope");
			}else{
				A_Refire();
			}
		}
		#### GAA 4 {
			A_MaserActionCheck();
			A_WeaponReady(WRF_NOFIRE);
			}
		#### A 0{
				if(invoker.weaponstatus[0]&RMF_ALT)A_StartSound("weapons/rubyendhi",CHAN_WEAPON);
				else A_StartSound("weapons/rubyendlo",CHAN_WEAPON);
				}
		goto ready;
	flash:
		THBF DE 0;
		#### F 0 A_CheckIdSprite("MASED0","MASEF0",PSP_FLASH);
		#### F 1;
		#### ED 1;
		#### F 1;
		#### EF 1;
		#### D 0;
		stop;
	altfire:
	firemode:
		#### A 1 offset(1,32) A_WeaponBusy();
		#### A 2 offset(2,32);
		#### A 1 offset(1,33);
		#### A 2 offset(0,34){
				if(invoker.weaponstatus[0]&RMF_ALT)A_StartSound("weapons/rubymaserpoweroff",8);
				else A_StartSound("weapons/rubymaserpoweron",8);
				}
		#### A 3 offset(-1,35);
		#### A 4 offset(-1,36);
		#### A 3 offset(-1,35);
		#### A 2 offset(0,34){
			invoker.weaponstatus[0]^=RMF_ALT;
			A_SetHelpText();
		}
		#### A 1;
		#### A 1 offset(0,34);
		#### A 1 offset(1,33);
		#### AAAAA 5 A_MaserActionCheck();
		goto nope;

	select0:
		MASE A 0{
			A_CheckIdSprite("MASEA0","MASEA0");
		}goto select0bfg;
	deselect0:
		MASE A 0 A_CheckIdSprite("MASEA0","MASEA0");
		#### A 0 A_Light0();
		#### A 0 A_StopSound();
		goto deselect0big;

	unload:
		#### A 0{
			invoker.weaponstatus[0]|=RMF_JUSTUNLOAD;
			if(invoker.weaponstatus[RMS_BATTERY]>=0){
				if(invoker.weaponstatus[0]&RMF_DOUBLEBAT){
					invoker.weaponstatus[0]|=RMF_BATSWAP;
					return resolvestate("unmag");
				}else return resolvestate("unmag");
			}return resolvestate("nope");
		}
	unmag:
		#### A 2 offset(0,33){
			A_SetCrosshair(21);
			A_MuzzleClimb(frandom(-1.2,-2.4),frandom(1.2,2.4));
		}
		#### A 3 offset(0,35);
		#### A 2 offset(0,40) A_StartSound("weapons/plasopen",8);
		#### A 0{
			if(PressingUse()&&invoker.weaponstatus[0]&RMF_DOUBLEBAT)return resolvestate("batswap");
			if(invoker.weaponstatus[0]&RMF_BATSWAP){
				if(invoker.weaponstatus[0]&RMF_DOUBLEBAT){
					A_MuzzleClimb(frandom(-1.2,-2.4),frandom(1.2,2.4));
					if(!PressingUnload()&&!PressingReload()&&!PressingAltReload()
						)return resolvestate("dropspare");
					return resolvestate("pocketspare");
				}else return resolvestate("altload");
			}else{
				int bat=invoker.weaponstatus[RMS_BATTERY];
				A_MuzzleClimb(frandom(-1.2,-2.4),frandom(1.2,2.4));
				if(
					bat<0
					||(!PressingUnload()&&!PressingReload())&&!PressingAltReload()
				)return resolvestate("dropmag");
				return resolvestate("pocketmag");
			}
		}
	dropmag:
		---- A 0{
			int bat=invoker.weaponstatus[RMS_BATTERY];
			invoker.weaponstatus[RMS_BATTERY]=-1;
			HDMagAmmo.SpawnMag(self,"HDBattery",bat);
		}goto magout;
	dropspare:
		---- A 0{
			int bat=invoker.weaponstatus[RMS_BATTERY2];
			invoker.weaponstatus[0]&=~RMF_DOUBLEBAT;
			invoker.weaponstatus[RMS_BATTERY2]=-1;
			if(bat<1)HDMagAmmo.SpawnMag(self,"HDBattery",0);
			else HDMagAmmo.SpawnMag(self,"HDBattery",bat);
		}goto magout;
	pocketmag:
		---- A 0{
			int bat=invoker.weaponstatus[RMS_BATTERY];
			invoker.weaponstatus[RMS_BATTERY]=-1;
			HDMagAmmo.GiveMag(self,"HDBattery",bat);
		}goto pocketend;
	pocketspare:
		---- A 0{
			int bat=invoker.weaponstatus[RMS_BATTERY2];
			invoker.weaponstatus[0]&=~RMF_DOUBLEBAT;
			invoker.weaponstatus[RMS_BATTERY2]=-1;
			if(bat<1)HDMagAmmo.GiveMag(self,"HDBattery",0);
			else HDMagAmmo.GiveMag(self,"HDBattery",bat);
		}goto pocketend;
	pocketend:
		#### A 8 offset(0,43) A_StartSound("weapons/pocket",9);
		#### A 8 offset(0,42) A_StartSound("weapons/pocket",9);
	magout:
		---- A 0 A_JumpIf(invoker.weaponstatus[0]&RMF_JUSTUNLOAD,"Reload3");
		---- A 0 A_JumpIf(invoker.weaponstatus[0]&RMF_BATSWAP,"altload");
		goto loadmag;

	reload:
		#### A 0{
			invoker.weaponstatus[0]&=~RMF_JUSTUNLOAD;
			if(
				invoker.weaponstatus[RMS_BATTERY]>19
				&&!(invoker.weaponstatus[0]&RMF_DOUBLEBAT)
				&&countinv("HDBattery")
			)setweaponstate("altreload");
			else if(
				invoker.weaponstatus[RMS_BATTERY]<20
				&&countinv("HDBattery")
			)setweaponstate("unmag");
		}goto nope;

	altreload:
		#### A 0{
			if(invoker.weaponstatus[RMS_BATTERY]<1)setweaponstate("reload");
			else if(invoker.weaponstatus[RMS_BATTERY2]>19)setweaponstate("nope");
			}
		#### A 0{
			invoker.weaponstatus[0]|=RMF_BATSWAP;
			}goto unmag;
	altload:
		#### A 0 A_JumpIf(invoker.weaponstatus[0]&RMF_EJECT,4);
		#### A 12 offset(0,42);
		#### A 2 offset(0,43){if(health>39)A_SetTics(0);}
		#### AA 2 offset(0,42);
		#### A 2 offset(0,44) A_StartSound("weapons/pocket",9);
		#### A 4 offset(0,43) A_StartSound("weapons/pocket",9);
		#### A 0 A_JumpIf(invoker.weaponstatus[0]&RMF_EJECT,2);
		#### A 6 offset(0,42);
		#### A 8 offset(0,38)A_StartSound("weapons/plasload",8);
		#### A 4 offset(0,37){if(health>39)A_SetTics(0);}
		#### A 4 offset(0,36)A_StartSound("weapons/plasclose",8);

		#### A 0{
			let mmm=HDMagAmmo(findinventory("HDBattery"));
			if(mmm){
				invoker.weaponstatus[RMS_BATTERY2]=mmm.TakeMag(true);
				invoker.weaponstatus[0]|=RMF_DOUBLEBAT;
			}
			if (PressingFire()&&invoker.weaponstatus[0]&RMF_EJECT){
				invoker.weaponstatus[0]&=~RMF_BATSWAP;
				setweaponstate("shoot");
			}
		}goto reload3;
	batswap:
		#### A 6 offset(0,42);
		#### A 1 offset(0,43) A_StartSound("weapons/pocket",9);
		#### A 2 offset(0,44) A_StartSound("weapons/pocket",9);
		#### A 3 offset(0,42);
		#### A 4 offset(0,38)A_StartSound("weapons/plasload",8);
		#### A 2 offset(0,36)A_StartSound("weapons/plasclose",8);
		#### A 0{
				int bat=invoker.weaponstatus[RMS_BATTERY];
				int bttwo=invoker.weaponstatus[RMS_BATTERY2];
				invoker.weaponstatus[RMS_BATTERY]=bttwo;
				invoker.weaponstatus[RMS_BATTERY2]=bat;
				if(invoker.weaponstatus[0]&RMF_EJECT){
					invoker.weaponstatus[0]&=~RMF_DOUBLEBAT;
					HDMagAmmo.SpawnMag(self,"HDBattery",bat);
					}
				}
		goto ready;
	loadmag:
		#### A 12 offset(0,42);
		#### A 2 offset(0,43){if(health>39)A_SetTics(0);}
		#### AA 2 offset(0,42);
		#### A 2 offset(0,44) A_StartSound("weapons/pocket",9);
		#### A 4 offset(0,43) A_StartSound("weapons/pocket",9);
		#### A 6 offset(0,42);
		#### A 8 offset(0,38)A_StartSound("weapons/plasload",8);
		#### A 4 offset(0,37){if(health>39)A_SetTics(0);}
		#### A 4 offset(0,36)A_StartSound("weapons/plasclose",8);

		#### A 0{
			let mmm=HDMagAmmo(findinventory("HDBattery"));
			if(mmm)invoker.weaponstatus[RMS_BATTERY]=mmm.TakeMag(true);
		}goto reload3;

	reload3:
		#### A 1 offset(0,40){
			A_StartSound("weapons/plasclose2",8);
			invoker.weaponstatus[0]&=~RMF_BATSWAP;
		}
		#### AAAAA 5 A_MaserActionCheck();
		#### A 2 offset(0,36);
		#### A 1 offset(0,33);
		#### AAAAA 5 A_MaserActionCheck();
		goto nope;

	user3:
		#### A 0 A_MagManager("HDBattery");
		goto ready;

	spawn:
		MASE Z -1;
		stop;
	}
	override void initializewepstats(bool idfa){
		weaponstatus[RMS_BATTERY]=20;
		weaponstatus[RMS_BATTERY2]=20;
		weaponstatus[0]|=RMF_DOUBLEBAT;
	}
	override void loadoutconfigure(string input){
		weaponstatus[0]|=RMF_DOUBLEBAT;
		int fm=getloadoutvar(input,"alt",1);
		if(!fm)weaponstatus[0]&=~RMF_ALT;
		else if(fm>0)weaponstatus[0]|=RMF_ALT;
		int eject=getloadoutvar(input,"eject",1);
		if(eject>=0)weaponstatus[0]|=RMF_EJECT;
	}
}
class EjectMaser:HDWeaponGiver{
	default{
		tag "Ruby Maser, Ejector Model";
		hdweapongiver.weapontogive "RubyMaser";
		hdweapongiver.config "eject";
	}
}
enum tbstatus{
	RMF_ALT=1,
	RMF_JUSTUNLOAD=2,
	RMF_DOUBLEBAT=4,
	RMF_EJECT=8,
	RMF_BATSWAP=16,

	RMS_FLAGS=0,
	RMS_BATTERY=1,
	RMS_BATTERY2=2,
	RMS_HEAT=3,
};

class MaserSpot:HDActor{
}

class MaserLow:IdleDummy{
	default{
		+puffonactors +hittracer +puffgetsowner +rollsprite +rollcenter +forcexybillboard
		renderstyle "add";
		obituary "%o was microwaved by %k's maser.";
	}
	double impactdistance;
	override void postbeginplay(){
		super.postbeginplay();

		double impactcloseness=1200;
		scale*=(impactcloseness)*0.0006;
		alpha=scale.y+0.3;
		vel=(frandom(-1,1),frandom(-1,1),frandom(1,3));

		int n=int(max(impactcloseness*0.02,2));
		int n1=n*3/5;
		int n2=n*2/5;
		if(tracer){
			HDF.Give(tracer,"Heat",n);
			int dmgflags=target&&target.player?DMG_THRUSTLESS:0;
			tracer.damagemobj(self,target,random(n1,n),"Electro",dmgflags);
		}
		pitch=frandom(80,90);
		angle=frandom(0,360);
		A_SpawnChunks("HDGunSmoke",clamp(n2*3/5,4,7),3,6);
		A_StartSound("weapons/rubybeam",0,CHANF_OVERLAP,0.2);
	}
	states{
	spawn:
		TNT1 A 1;
		stop;
	}
}
class MaserHigh:MaserLow{
	override void postbeginplay(){
		if(impactdistance>6000){
			destroy();
			return;
		}
		super.postbeginplay();

		double impactcloseness=2000-impactdistance;
		scale*=(impactcloseness)*0.0128;

		int n=int(max(impactcloseness*0.01,2));
		int n1=n*3/5;
		int n2=n*2/5;
		if(tracer){
			HDF.Give(tracer,"Heat",n*3);
			int dmgflags=target&&target.player?DMG_THRUSTLESS:0;
			tracer.damagemobj(self,target,random(n1,n)/10,"Electro",dmgflags);
		}
		A_AlertMonsters();
	}
}

class MaserFrag:HDActor{
	default{
		+forcexybillboard +bright
		radius 0;height 0;speed 2;
		renderstyle "add";alpha 0.4; scale 0.24;
	}
	states{
	spawn:
		BFE2 DDDDDDDDDD 1 bright nodelay {
			A_FadeIn(0.1);
			A_SetTranslation("AllRed");
			}
		BFE2 D 1 A_FadeOut(0.9);
		wait;
	}
}

class MaserSpawn:IdleDummy{
	states{
	spawn:
		TNT1 A 0 nodelay{
			let zzz=RubyMaser(spawn("RubyMaser",pos,ALLOW_REPLACE));
			if(!zzz)return;
			HDF.TransferSpecials(self, zzz);
			if(!random(0,1))zzz.weaponstatus[0]|=RMF_EJECT;
		}stop;
	}
}
