AddOptionMenu "OptionsMenu"
{
	Submenu "Peppergrinder", "PeppergrinderMenu"
}

AddOptionMenu "HDAddonMenu"
{
	Submenu "Peppergrinder", "PeppergrinderMenu"
}

OptionMenu "PeppergrinderMenu"
{
	Title "Peppergrinder Options"
	
	StaticText "----- Gear Shortcuts -----", "Teal"
	Control "Binoculars","use HDBinoculars"
	Control "Ammo Conversion Device","use TheBox"
	Control "Zorcher","use HDZorcher"
	StaticText "" //line ender

	// [Ace] You can have up to 32 of these. On a single CVar, that is.
	// Would have been 64 if GZDoom integers were 8-bytes but oh well.
	StaticText "----- Weapon Spawns -----", "Teal"
	PepperWeapon "Otis-5", 0
	PepperWeapon "Trog", 1
	PepperWeapon "HLAR", 2
	PepperWeapon "Lotus Carbine", 3
	PepperWeapon "Box Cannon", 4
	PepperWeapon "Ruby Maser", 5
	PepperWeapon "Guillotine", 6
	PepperWeapon "Irons Liberator", 7
	PepperWeapon "Scoped Slayer", 8
	PepperWeapon "Helzing", 9
	PepperWeapon "Greely", 10
	PepperWeapon "Sawed-Off Slayer", 11
	PepperWeapon "Vera", 12
	PepperWeapon "Wiseau", 13
	PepperWeapon "Lisa", 14
	PepperWeapon "Oddball", 15
	PepperWeapon "Bastard", 16
	PepperWeapon "Scoped Revolver", 17
	PepperWeapon "BreakerTek P90", 18
	PepperWeapon "Mauler", 19
	PepperWeapon "Plasma Pistol", 20
	PepperWeapon "Yeyuze Grenade Launcher", 21
	PepperWeapon "Zorcher", 22
	PepperWeapon "BPX Pistol Caliber Carbine", 23
	PepperWeapon "ZM94 Anti-Materiel Rifle", 24
	PepperWeapon "Fulgur Laser Machinegun", 25
	PepperWeapon "Baileya Repeater 5", 26
	PepperWeapon "Aurochs .451 Revolver", 27
	PepperWeapon "PG667 Designated Marksman Rifle", 28
	PepperWeapon "Yurei High-Capacity SMG", 29
	PepperWeapon "Coyote .451 Automag", 30
	PepperWeapon "Killer7 .500 Magnum", 31
	PepperWeapon "Brahmastra-500 Rifle", 32

	Command "Enable All", "PPR_EnableAll_Weapons"
	Command "Disable All", "PPR_DisableAll_Weapons"
	StaticText ""
	StaticText "----- Ammo Spawns -----", "Teal"
	PepperAmmo ".500 S&W", 0
	PepperAmmo "Trog Mags", 1
	PepperAmmo "9mm Clips", 2
	PepperAmmo "NDM Ammo", 3
	PepperAmmo ".355 Mags", 4
	PepperAmmo "12 Gauge Slugs", 5
	PepperAmmo "ZM94 Mags", 6
	PepperAmmo "BreakerTek P90 Mags", 7
	PepperAmmo ".451 Frei Boxes", 8
	PepperAmmo ".066 Bore Boxes", 9
	PepperAmmo "Yurei Mags", 10
	PepperAmmo "Coyote .451 Magazines", 11
	PepperAmmo "Killer7 .500 Magnum Magazines", 12
	PepperAmmo "20mm Grenades", 13
	PepperAmmo "10-round .500 Heavy Magazine", 14
	PepperAmmo "Slug Shell Speedloader", 15

	Command "Enable All", "PPR_EnableAll_Ammo"
	Command "Disable All", "pepper_ammospawns_1 0"
}
